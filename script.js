function filterBy (array, dataType){
    let filterResult = [];
    array.filter(element => {
        if (typeof element !== dataType){
            filterResult.push(element)
        }
    });
    return filterResult;
}
console.log(filterBy(['hello', undefined, 'world', 23, '23', null, true, {name: "Rick", age: 23}, [1,2,3], 123456789012345678n], 'string'));